using Corrupted;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class CaseItem : BoolValue
{

    [Header("Bio")]
    public Image icon;
    public string title;
    [TextArea]
    public string description;

    //[Header("Settings")]
    //public BoolVariable acquired;
    
}
