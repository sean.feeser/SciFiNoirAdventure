using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Case Note", menuName = "SciFiNoir/CaseNote")]
public class CaseNote : CaseItem
{



    public CaseItem[] relevantItems;

}
