using Corrupted;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Character", menuName = "SciFiNoir/Character", order = 0)]
public class Character : CaseItem
{

    


    [Header("Stats")]
    public float money;
    public float reputation;
    public float morale;
    public float empathy;


    [Header("Dialouge")]
    public List<CharacterGraph> greetings = new List<CharacterGraph>();
    public List<CharacterGraph> questions = new List<CharacterGraph>();
    public List<CharacterGraph> outro = new List<CharacterGraph>();

    [Header("Memories")]
    public CharacterMemory[] memories;


    public CharacterGraph[] GetQuestions()
    {
        List<CharacterGraph> output = new List<CharacterGraph>();
        foreach(CharacterGraph cg in questions)
        {
            if (cg.available)
                output.Add(cg);
        }
        return output.ToArray();
    }

    public CharacterGraph GetGreeting()
    {
        return GetPriorityGraph(greetings);
    }

    public CharacterGraph GetOutro()
    {
        return GetPriorityGraph(outro);
    }

    CharacterGraph GetPriorityGraph(List<CharacterGraph> graph)
    {
        float highestPriority = 0;
        CharacterGraph? highestCG = null;
        foreach(CharacterGraph cg in graph)
        {
            if (cg.available == false)
                continue;
            if(cg.priority >= highestPriority || highestCG == null)
            {
                highestCG = cg;
                highestPriority = cg.priority;
            }
        }
        if (highestCG == null)
            Debug.LogError("Character: Missing available graphs!!", this);
        return (CharacterGraph)highestCG;
    }

}
[System.Serializable]
public struct CharacterGraph
{
    public BoolVariable available;
    public SequenceGraph graph;
    public float priority;
    public bool hasPlayed;
}


[System.Serializable]
public struct CharacterMemory
{
    public BoolVariable active;
    public string label;
    [TextArea]
    public string description;

}