using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using XNode;


namespace Corrupted
{

    /// <summary> Base node for the DialogueGraph system </summary>
    public abstract class CorruptedNode : Node
    {
        [Input] public CorruptedNode input;
        [Output] public CorruptedNode output;

        public string key;
        public Action onStateChange;
        public static Action OnAnyChangeClear, OnDChangeRedraw, OnRChangeRedraw;

        public abstract bool led { get; }

        public CorruptedNode[] path
        {
            get
            {
                return new List<CorruptedNode>(GetPort("output").GetInputValues<CorruptedNode>()).ToArray();
            }
        }

        public abstract IEnumerator PlayNode(GraphSequence view, CorruptedBehaviour runner);


        public CorruptedNode[] GetPath(string port)
        {
            return new List<CorruptedNode>(GetPort(port).GetInputValues<CorruptedNode>()).ToArray();
        }

        public CorruptedNode GetLink(string port)
        {
            foreach (CorruptedNode gn in GetPath(port))
            {
                if (gn != null)
                    return gn;
            }
            Debug.LogError("GraphNode: Port " + port + " has no link!");
            return null;
        }

        public override object GetValue(NodePort port)
        {
            return output;
        }

        public void SendSignal(NodePort output)
        {
            // Loop through port connections
            int connectionCount = output.ConnectionCount;
            for (int i = 0; i < connectionCount; i++)
            {
                NodePort connectedPort = output.GetConnection(i);

                // Get connected ports logic node
                CorruptedNode connectedNode = connectedPort.node as CorruptedNode;

                // Trigger it
                if (connectedNode != null) connectedNode.OnInputChanged();
            }
            if (onStateChange != null) onStateChange();
            //OnAnyChange();
        }

        protected virtual void OnInputChanged() { }

        //public virtual void OnChangeClear() { }
        //public virtual void OnChangeRedraw() { }

        //public static void OnAnyChange()
        //{
        //    if (OnAnyChangeClear != null) OnAnyChangeClear();
        //    if (OnRChangeRedraw != null) OnDChangeRedraw();
        //    if (OnRChangeRedraw != null) OnRChangeRedraw();
        //}

        public override void OnCreateConnection(NodePort from, NodePort to)
        {
            OnInputChanged();
        }
    }
}
