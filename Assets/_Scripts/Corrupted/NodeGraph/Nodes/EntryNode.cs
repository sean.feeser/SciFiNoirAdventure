using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using XNode;

namespace Corrupted
{

    /// <summary> Base node for the DialogueGraph system </summary>
    [NodeWidth(100), NodeTint(50, 100, 50)]
    public class EntryNode : CorruptedNode
    {
        public int visit = 0;
        //[Output] public DialogueNode output;

        //public CorruptedNode[] path
        //{
        //    get
        //    {
        //        return new List<CorruptedNode>(GetPort("output").GetInputValues<CorruptedNode>()).ToArray();
        //    }
        //}
        public CorruptedNode link
        {
            get
            {
                foreach (CorruptedNode gn in path)
                {
                    if (gn == null)
                    {
                        Debug.LogError("DialogueView: Entrynode path has null element");
                        continue;
                    }
                    return gn;
                }
                Debug.LogError("DialogueView: Entry node has no link!");
                return null;
            }
        }

        public override bool led { get { return true; } }


        public override IEnumerator PlayNode(GraphSequence view, CorruptedBehaviour runner)
        {
            Debug.Log("DialogueView: Entry node links to " + link.name);
            yield return view.UpdateSequence(link, runner);
        }

    }
}
