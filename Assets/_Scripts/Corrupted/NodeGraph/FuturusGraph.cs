using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Corrupted
{
    [CreateAssetMenu(fileName = "NodeGraph", menuName = "Corrupted/Graph", order = 1)]
    public class CorruptedGraph : NodeGraph
    {

    }
}