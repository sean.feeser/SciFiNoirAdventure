using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DialogueView))]
public class CharacterView : SequenceBehaviour
{

    public Character character;

    DialogueView view;

    public override void Start()
    {
        base.Start();
        view = GetComponent<DialogueView>();
    }



    public CharacterGraph[] PlayQuestions(int page = 4, int start = 0)
    {
        CharacterGraph[] output = character.GetQuestions();
        int p = 0;
        for (int i = start; i < output.Length; i++) { 
            CharacterGraph cg = output[i];
            sequence.UpdateSequence(cg.graph.GetEntry());
            if(page > 0)
            {
                if(i > page + start)
                {
                    view.AddResponse("I have more questions...", () =>
                    {
                        PlayQuestions(page, p);
                    });
                }
                break;
            }
        }
        return output;
    }

    public CharacterGraph PlayGreeting()
    {
        CharacterGraph output = character.GetGreeting();
        sequence.UpdateSequence(output.graph.GetEntry());
        return output;
    }

    public CharacterGraph PlayOutro()
    {
        CharacterGraph outro = character.GetOutro();
        sequence.UpdateSequence(outro.graph.GetEntry());
        return outro;
    }

    public IEnumerator AwaitGreeting()
    {
        CharacterGraph greeting = PlayGreeting();
        yield return new WaitWhile(() => greeting.graph.inProgress);
    }

    public IEnumerator AwaitOutro()
    {
        CharacterGraph outro = PlayOutro();
        yield return new WaitWhile(() => outro.graph.inProgress);
    }

    public IEnumerator AwaitQuestions()
    {
        CharacterGraph[] questions = PlayQuestions();

        bool inProgress = true;
        while (inProgress)
        {
            inProgress = false;//We're searching for items in progress, so let's starts false
            foreach(CharacterGraph cg in questions)
            {
                if (cg.graph.inProgress)
                {
                    inProgress = true;//We found something in progress! We can stop now.
                    break;
                }
            }
            yield return null;
        }
    }

}
