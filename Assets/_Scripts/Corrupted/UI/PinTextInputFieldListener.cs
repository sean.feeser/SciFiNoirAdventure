using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Corrupted.Autohands
{

    [RequireComponent(typeof(TMP_InputField))]
    public class PinTextInputFieldListener : CorruptedBehaviour
    {

        PinTextInputField root;

        TMP_InputField text;


        public bool IsFocused
        {
            get
            {
                if (text == null)
                    text = GetComponent<TMP_InputField>();
                return text.isFocused;
            }
        }

        // Start is called before the first frame update
        public override void Start()
        {
            text = GetComponent<TMP_InputField>();
            //text.onSelect.AddListener(SetInputPosition);
        }

        public void SetCharacter(char c)
        {
            if (text == null)
                text = GetComponent<TMP_InputField>();
            text.text = c.ToString();
        }

        public void Clear()
        {
            if (text == null)
                text = GetComponent<TMP_InputField>();
            text.text = "";
        }

        public int GetIndex()
        {
            if (root == null)
                root = GetComponentInParent<PinTextInputField>();
            Transform t = transform;
            while(t.parent != null)
            {
                //Debug.Log($"PinTextInputField: {name} is searching {t.name}'s parent {t.parent.name} equals {root.name}", t.gameObject);

                if (t.parent == root.transform)
                {
                    return t.GetSiblingIndex();
                }
                t = t.parent;
            }
            Debug.LogError("PinTextInputField: Listener has no root behaviour!");
            return -1;
        }

    }
}