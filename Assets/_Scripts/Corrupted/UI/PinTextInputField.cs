using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Corrupted.Autohands
{

    [RequireComponent(typeof(HorizontalLayoutGroup))]
    public class PinTextInputField : KeyboardListener
    {

        PinTextInputFieldListener[] inputFields;

        [SerializeField]
        int inputPosition;

        [SerializeField]
        string inputText = "";

        public UnityEvent<string> OnInputUpdated, OnInputCompleted;

        public override bool IsFocused
        {
            get
            {
                if (inputFields == null)
                    PopulateInputFields();
                foreach(PinTextInputFieldListener l in inputFields)
                {
                    if (l.IsFocused)
                        return true;
                }
                return false;
            }
        }

        // Start is called before the first frame update
        public override void Start()
        {
            PopulateInputFields();
            inputPosition = 0;
        }
        

        protected override void OnUpdateInput(string input)
        {
            if (inputPosition >= inputFields.Length)
                return;
            inputText = inputText.Insert(inputPosition, input);
            UpdateInputField();
            inputPosition += input.Length;
            if(inputPosition >= inputFields.Length)
            {
                OnInputCompleted?.Invoke(inputText);
            }
        }

        protected override void OnBackspaceInput()
        {
            if (inputText.Length == 0 || inputPosition == 0)
            {
                inputPosition = 0;
                return;
            }
            inputText = inputText.Remove(inputPosition - 1);
            inputPosition--;
            UpdateInputField();
        }

        
        public void SetValue(string input)
        {
            if (inputFields == null)
                PopulateInputFields();
            char[] inputText = input.ToCharArray();
            for (int i = 0; i < inputFields.Length; i++)
            {
                if (i < inputText.Length)
                {
                    inputFields[i].SetCharacter(inputText[i]);
                }
                else
                {
                    inputFields[i].Clear();
                }
            }
            //Debug.Log($"PinTextInputField: Set value to {input}");
        }

        public string GetValue()
        {
            return inputText;
        }

        public void SetCursor(PinTextInputFieldListener index)
        {
            for (int i = 0; i < inputFields.Length; i++)
            {
                if(inputFields[i] == index)
                {
                    inputPosition = i;
                    return;
                }
            }
            Debug.LogError($"PinTextInputField: {index.name} is not a valid listener under {name}");
        }


        [Button]
        public void PopulateInputFields()
        {
            PinTextInputFieldListener[] listeners = GetComponentsInChildren<PinTextInputFieldListener>();
            inputFields = new PinTextInputFieldListener[listeners.Length];
            foreach (PinTextInputFieldListener child in listeners)
            {
                inputFields[child.GetIndex()] = child;
            }
            //inputFields = listeners.ToArray();

#if UNITY_EDITOR
            for (int i = 0; i < inputFields.Length; i++)
            {
                inputFields[i].name = "Pin Input Column " + i;
            }
            Debug.Log($"PinTextInputField: Named {inputFields.Length} children");
#endif
        }

        [Button]
        public void UpdateInputField()
        {
            SetValue(inputText);
            OnInputUpdated?.Invoke(inputText);
        }
    }

    

}
