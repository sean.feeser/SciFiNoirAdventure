using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Corrupted
{

    [CreateAssetMenu(fileName = "GraphSequence", menuName = "Corrupted/Sequences/Graph")]
    public class GraphSequence : CorruptedSequence
    {
        public CorruptedGraph graph;

        int visits = 0;


        bool inProgress
        {
            get
            {
                return current != null;
            }
        }

        CorruptedNode current;


        public override IEnumerator Sequence(CorruptedBehaviour behaviour)
        {
            yield return UpdateSequence(Reset(), behaviour);
        }

        public IEnumerator UpdateSequence(CorruptedNode node, CorruptedBehaviour runner)
        {
            if (node == null)
            {
                Debug.LogError("DialogueView: Null node received!");
            }
            else
            {
                yield return node.PlayNode(this, runner);
            }
        }

        public CorruptedNode Resume()
        {
            return current;
        }

        public void Stop()
        {
            current = null;
        }

        public CorruptedNode Reset()
        {
            if (graph == null)
                return null;
            if (inProgress)
            {
                return Resume();
            }
            EntryNode initial = null;
            bool exactFound = false;
            List<EntryNode> nodes = new List<EntryNode>();
            EntryNode highestVisit = null;
            foreach (XNode.Node n in graph.nodes)
            {
                if (n is EntryNode)
                {
                    EntryNode en = (EntryNode)n;
                    if (en.visit == visits)
                    {
                        exactFound = true;
                        initial = en;
                        break;
                    }
                    else if (highestVisit == null || en.visit > highestVisit.visit)
                    {
                        highestVisit = en;
                    }
                }
            }
            if (initial != null && exactFound)
            {//We found our exact entry
             //UpdateView(initial.link);
                return initial;
            }
            else if (highestVisit != null)//We haven't found our exact entry so let's use the highest count
            {
                //UpdateView(highestVisit.link);
                initial = highestVisit;
                return initial;
            }
            else
            {
                Debug.LogError("DialogueView: Failed to find entry node... please ensure one exists in your dialogue graph!");
                return null;
            }
        }
    }
}
