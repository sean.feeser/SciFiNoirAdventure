using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Corrupted
{

    [CreateAssetMenu(fileName = "DestructionSequence", menuName = "Corrupted/Sequences/Destruction")]
    public class DestructionSequence : CorruptedSequence
    {
        public GameObjectVariable effect;
        public FloatVariable DestroyAfterTime = 1f;

        public override IEnumerator Sequence(CorruptedBehaviour behaviour)
        {
            if(effect.HasValue)
                Instantiate(effect, behaviour.transform.position, behaviour.transform.rotation);
            yield return new WaitForSeconds(DestroyAfterTime);
            Destroy(behaviour.gameObject);
        }
    }
}
