using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(NarratorStackNode))]
public class NarratorNodeEditor : NodeEditor
{
    public override void AddContextMenuItems(GenericMenu menu)
    {
        base.AddContextMenuItems(menu);
        menu.AddItem(new GUIContent("Convert to Subtitle"), false, NodeEditorWindow.current.ChangeNodeType<NarratorStackNode, NarratorSubtitleNode>);
    }

}
