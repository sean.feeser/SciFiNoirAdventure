using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[NodeWidth(200), NodeTint(100, 70, 120), CreateNodeMenu("SciFiNoir/Character/Outro")]
public class CharacterOutroNode : GraphNode
{
    public override bool led => true;

    public override IEnumerator PlayNode(SequenceSystemManager director)
    {
        CharacterView view = director.GetBehaviour<CharacterView>();
        if (view != null)
        {
            yield return view.AwaitOutro();
        }
        PlayNextInPath(director);
    }
}
