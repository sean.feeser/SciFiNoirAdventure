using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Corrupted.Autohands;


namespace Corrupted.Autohands
{

    public class DestroyOnPressed : HandTouchEventListener
    {

        public GameObject toDestroy;
        public float delay = 0f;


        public override void OnButtonActivated()
        {
            if (toDestroy == null) toDestroy = gameObject;
            Destroy(toDestroy, delay);
        }

    }
}