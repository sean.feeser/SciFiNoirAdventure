using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Corrupted;
using Autohand;
//using Corrupted.XR;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "HapticsManager", menuName = "Corrupted/XR/Haptics")]
public class HapticsManager : CorruptedModel
{
    Hand leftHand, rightHand;
    public int defaultIndex;
    public HapticsType[] haptics;

    //void GetHands()
    //{
    //    leftHand = Hand.leftHand;
    //    rightHand = Hand.rightHand;
    //}

    //HapticsType GetHaptics(string name)
    //{
    //    foreach(HapticsType type in haptics)
    //    {
    //        if (type.name == name)
    //            return type;
    //    }
    //    Debug.LogError("HapticsManager: No haptics match the name " + name);
    //    return new HapticsType();
    //}


    //public void PlayHaptics(Handedness hand, float duration, float amplitude)
    //{
    //    GetHands();
    //    if(hand == Handedness.Left || hand == Handedness.Both)
    //    {
    //        leftHand.PlayHapticVibration(duration, amplitude);
    //    }
    //    if (hand == Handedness.Right || hand == Handedness.Both)
    //    {
    //        rightHand.PlayHapticVibration(duration, amplitude);
    //    }
    //}

    //public void PlayHaptics(Handedness hand, int hapticsType)
    //{
    //    HapticsType type = haptics[hapticsType];
    //    PlayHaptics(hand, type.duration, type.amplitude);
    //}

    //public void PlayHaptics(Handedness hand, string hapticsType)
    //{
    //    HapticsType type = GetHaptics(hapticsType);
    //    PlayHaptics(hand, type.duration, type.amplitude);
    //}

    //public void PlayHaptics(Handedness hand)
    //{
    //    PlayHaptics(hand, defaultIndex);
    //}

    //public void PlayHaptics()
    //{
    //    PlayHaptics(Handedness.Both, defaultIndex);
    //}

    //[Button]
    //public void PlayHapticsLeft()
    //{
    //    PlayHaptics(Handedness.Left, defaultIndex);
    //}

    //[Button]
    //public void PlayHapticsRight()
    //{

    //    PlayHaptics(Handedness.Right, defaultIndex);
    //}

    //public void PlayHaptics(Hand h, Grabbable g)
    //{
    //    Handedness lr = h.left ? Handedness.Left : Handedness.Right;
    //    PlayHaptics(lr, defaultIndex);
    //}




}
[System.Serializable]
public struct HapticsType
{
    public string name;
    public float duration;
    public float amplitude;
}
